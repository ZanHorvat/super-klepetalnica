/*global $*/
/*global io*/
/*global Klepet*/

/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Iskanje povezav na slike in prikazovanje le teh na koncu
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo slike
 */
function dodajSlike(vhodnoBesedilo) {
  
  console.log("dodajSlike()");
  
  vhodnoBesedilo = vhodnoBesedilo.split(" ");
  
  var slika = new RegExp(/https?:\/\/[^ ]+?(?:\.jpg|\.png|\.gif)/);
  
  var dolzinaPovedi = vhodnoBesedilo.length;
  
  for(var i = 0; i < dolzinaPovedi; i++){
      if(vhodnoBesedilo[i].search(slika) > -1){
          
          var vnos = vhodnoBesedilo[i].match(slika)[0];
          vhodnoBesedilo.push("<br /><img style='width: 200px; margin-left: 20px;' src='" + vnos + "' />");
      }
  }
  
  vhodnoBesedilo = vhodnoBesedilo.join(" ");
  
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  
  sporocilo = sporocilo.split(":");
  
  if(orgNadimki[sporocilo[0]] !== undefined){
    sporocilo[0] = orgNadimki[sporocilo[0]] + "("+sporocilo[0]+")";
  }
  sporocilo = sporocilo.join(":");
  
  sporocilo = sporocilo.split(" ");
  for(var i = 0; i < sporocilo.length; i++)
    if(orgNadimki[sporocilo[i]] !== undefined){
      sporocilo[i] = orgNadimki[sporocilo[i]] + "("+sporocilo[i]+")";
    }
  sporocilo = sporocilo.join(" ");
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;

  var zapisSlike = new RegExp(/https?:\/\/[^ ]+?(?:\.jpg|\.png|\.gif)/);
  var jeSlika = sporocilo.search(zapisSlike) > -1;

  var jeKrc = sporocilo.indexOf("&#") > -1;
  
  if (jeSmesko || jeSlika || jeKrc) {
    sporocilo = sporocilo.split("<").join("&lt;")
                         .split(">").join("&gt;")
                         .split("&lt;img").join("<img")
                         .split("png' /&gt;").join("png' />")
                         .split("jpg' /&gt;").join("jpg' />")
                         .split("gif' /&gt;").join("gif' />")
                         .split("&lt;br /&gt;").join("<br />");
                         

    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var orgNadimki = {};

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  
  socket.on('posredujPreimenovanje', function(nadimek) {
    orgNadimki[nadimek.from] = nadimek.to;
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {

    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      
      var vzdevek = uporabniki[i];
      if(orgNadimki[vzdevek] !== undefined){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(orgNadimki[vzdevek] + " (" + vzdevek+")"));
      } else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
      
    
    }

    $('#seznam-uporabnikov div').click(function() {
      
      var naslovnik = $(this).text();
      
      naslovnik = naslovnik.split("(");
      
      if(naslovnik.length > 1){
        naslovnik[1] = naslovnik[1].replace(")","");
        
        $('#poslji-sporocilo').val('/zasebno \"' + naslovnik[1] + '\" \"&#9756;\"');
      } else {
        naslovnik = naslovnik.join("");
        $('#poslji-sporocilo').val('/zasebno \"' + $(this).text() + '\" \"&#9756;\"');
      }
      
      
      $('#poslji-sporocilo').focus();
      procesirajVnosUporabnika(klepetApp, socket);
      
    });

  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 500);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
